===============================
meteocodes
===============================

.. image:: https://img.shields.io/travis/peter9192/meteocodes.svg
        :target: https://travis-ci.org/peter9192/meteocodes

.. image:: https://img.shields.io/pypi/v/meteocodes.svg
        :target: https://pypi.python.org/pypi/meteocodes


A random collection of meteorological functions, serving mostly as an exercise in setting up a well-structured scientific coding project.

* Free software: 3-clause BSD license
* Documentation: (COMING SOON!) https://peter9192.github.io/meteocodes.

Features
--------

* TODO
